/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <functional>

#include <ignition/math.hh>
#include "gazebo/physics/physics.hh"
#include "/usr/include/sdformat-6.2/sdf/Element.hh"
#include "Actor_Plugin_BVH.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(Actor_Plugin_BVH)

#define WALKING_ANIMATION "walking"
using namespace std;
/////////////////////////////////////////////////
Actor_Plugin_BVH::Actor_Plugin_BVH()
{
}

/////////////////////////////////////////////////
void Actor_Plugin_BVH::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
//void Actor_Plugin_BVH::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) //load actor plugin
{
  this->world = _world;   // pointer to the plugin's sdf's elements:<plugin name="actor1_plugin" filename="libActorPlugin.so">
  //this->actor = boost::dynamic_pointer_cast<physics::Actor>(_model);  //_model pointer to the parent model:<actor name="actor1">
  //this->world = this->actor->GetWorld();
  actorName = "actor";
  pose = ignition::math::Pose3d(1.2, 1.4, 1.218, 0, 0, 0);
  skinfile = "walk.dae";
  scale = 1.00000;
  
  AddActor(_world, actorName, pose, skinfile, scale);

} 

//////////////////////
void Actor_Plugin_BVH::AddActor(const physics::WorldPtr &_world, const std::string &_actorName, 
    const ignition::math::Pose3d &_pose, const std::string &_skinfile, double &_scale)
{
  sdf::ElementPtr actorElem = _world->AddElement("actor");
  
  actorElem->GetAttribute("name")->Set(_actorName);
  sdf::ElementPtr PoseElem = actorElem->GetElement("pose");
  PoseElem->Set(_pose);
  sdf::ElementPtr skinElem = actorElem->GetElement("skin");
  sdf::ElementPtr skinFileName = skinElem->GetElement("filename");
  skinFileName->Set(_skinfile);
  sdf::ElementPtr skinScale = skinElem->GetElement("scale");
  skinScale->Set(_scale);

  /* 
  this->sdf->AddElement("Animation");
  sdf::ElementPtr aniElem = this->sdf->GetElement("Animation");
  aniElem->GetAttribute("name")->Set("walking");
  sdf::ElementPtr fileName = aniElem->GetElement("filename");
  fileName->Set("~/home/luo/Downloads/02_05.bvh"); 
  sdf::ElementPtr Scale = aniElem->GetElement("scale");
  Scale->Set(1.00000);
  sdf::ElementPtr interpl_x = aniElem->GetElement("interpolate_x");
  interpl_x->Set(true);
 */
}
 