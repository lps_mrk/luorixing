/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <functional>

#include <ignition/math.hh>
#include "gazebo/physics/physics.hh"
//#include "/usr/include/sdformat-6.2/sdf/Element.hh"
#include "AddAnimBVH.hh"
//#include "/home/luo/Downloads/gazebo/deps/tinyxml2/gazebo/tinyxml2.h"

using namespace gazebo;
GZ_REGISTER_MODEL_PLUGIN(AddAnimBVH)

#define WALKING_ANIMATION "walking"
using namespace std;
/////////////////////////////////////////////////
AddAnimBVH::AddAnimBVH()
{
}

/////////////////////////////////////////////////
//void AddAnimBVH::Load(sdf::ElementPtr _sdf)
//void AddAnimBVH::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
void AddAnimBVH::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) //load actor plugin
{
  
  this->sdf = _sdf;   // pointer to the plugin's sdf's elements:<plugin name="actor1_plugin" filename="libActorPlugin.so">
  
  this->actor = boost::dynamic_pointer_cast<physics::Actor>(_model);  //_model pointer to the parent model:<actor name="actor1">

 
  //this->world = this->actor->GetWorld();
 // actorName = "actor";
  //pose = ignition::math::Pose3d(1.2, 1.4, 1.218, 0, 0, 0);
  //skinfile = "walk.dae";
  //scale = 1.00000;
  
  //* 这句话用于验证_model是actor,
 // string aaa = this->actor->GetName();//actor是平躺着的
  //string aaa = _model->GetName(); //actor是站立的
  //cout<<aaa<<endl;
  //



  
  //this->connections.push_back(event::Events::ConnectWorldUpdateBegin(
     //    std::bind(&AddAnimBVH::OnUpdate, this, std::placeholders::_1)));
  //cout<<actorElem->GetName()<<endl;
  //cout << *(actorElem->GetElement("animation")->GetAttribute("name"))<<endl;
  /// this->actor->Update();
  sdf::ElementPtr actorElem = this->sdf->GetParent();
  skinFile = "walk.dae";
  skinScale = 1.00000;
  animFile ="walk.dae";
  animScale = 1.000000;
  interPLX = true;
  type1 = "walking";

  ignition::math::Pose3d actorPose = ignition::math::Pose3d(ignition::math::Vector3d(1, 5, 10.218),ignition::math::Quaterniond(1.5707,0,0));
  sdf::ElementPtr poseElem = actorElem->AddElement("pose");
  poseElem->Set(actorPose);

  auto skinElem = actorElem->GetElement("skin");
  skinElem->GetElement("filename")->Set(skinFile);
  skinElem->GetElement("scale")->Set(skinScale);
  this->actor->Load(actorElem);


  //auto animElem = this->model->GetElement("animation");
  //animElem->GetAttribute("name")->Set(type1);
  //animElem->GetElement("filename")->Set(animFile);

  
  //animElem->GetElement("scale")->Set(animScale);
  //animElem->GetElement("interpolate_x")->Set(interPLX);
  //cout<<poseElem->GetDescription()<<endl;
 /*  cout<<actorElem->GetElementDescriptionCount()<<endl;
  cout<<actorElem->GetDescription()<<endl;
  cout<<skinElem->GetDescription()<<endl;
  cout<<animElem->GetDescription()<<endl;
  cout<<*(animElem->GetAttribute("name"))<<endl;
  */

  //actorElem->GetElement("skin")->GetElement("filename")->Set<std::string>("walk.dae");
 // actorElem->GetElement("skin")->GetElement("scale")->Set<double>(1.000000);

  
  


  //bool aa = actorElem->HasElement("skin");
  //sdf::ElementPtr animElem = actorElem->AddElement("Animation");
  //animElem->GetAttribute("name")->Set("walking");
 // bool bb = actorElem->HasElement("Animation");
 // cout << bb <<endl;

  //cout << aa <<endl;
  /*
  //physics::BasePtr actorElem = _world->BaseByName("actor");
  actorElem->GetAttribute("name")->Set("actor12");
  sdf::ElementPtr skinElem = actorElem->AddElement("skin");
  sdf::ElementPtr skinFileElem = skinElem->AddElement("filename");
  skinFileElem->Set("walk.dae");
  sdf::ElementPtr skinScaleElem = skinElem->AddElement("scale");
  skinScaleElem->Set(1.0);
  ignition::math::Pose3d actorPose = ignition::math::Pose3d(ignition::math::Vector3d(1,0,1.218),ignition::math::Quaterniond(1.5707,0,0));
  sdf::ElementPtr poseElem = actorElem->AddElement("pose");
  poseElem->Set(actorPose);
  cout<<actorElem->GetName()<<endl;
  cout <<actorElem->GetAttribute("name") <<endl;
 */
  
 


} 
/*void AddAnimBVH::OnUpdate(const common::UpdateInfo &_info)
{
  
  auto actorElem = this->sdf->GetParent();
  //ignition::math::Pose3d actorPose = ignition::math::Pose3d(ignition::math::Vector3d(1, 5, 10.218),ignition::math::Quaterniond(1.5707,0,0));
  sdf::ElementPtr animElem = actorElem->GetElement("animation");
  animElem->GetElement("filename")->Set("moonwalk.dae");
  this->actor->Update();
  
  

  //this->lastUpdate = _info.simTime;

}
 
//////////////////////
void AddAnimBVH::AddActor(sdf::ElementPtr &_linkSdf, std::string &_type, 
    std::string &_animFile, double &_scale, bool &_interPL)
{
  
  sdf::ElementPtr actorElem = _world->AddElement("actor");
  
  actorElem->GetAttribute("name")->Set(_actorName);
  sdf::ElementPtr PoseElem = actorElem->GetElement("pose");
  PoseElem->Set(_pose);
  sdf::ElementPtr skinElem = actorElem->GetElement("skin");
  sdf::ElementPtr skinFileName = skinElem->GetElement("filename");
  skinFileName->Set(_skinfile);
  sdf::ElementPtr skinScale = skinElem->GetElement("scale");
  skinScale->Set(_scale);

  
  
  sdf::ElementPtr aniElem = _linkSdf->AddElement("Animation");
  aniElem->GetAttribute("name")->Set(_type);
  sdf::ElementPtr fileName = aniElem->GetElement("filename");
  fileName->Set(_animFile); 
  sdf::ElementPtr Scale = aniElem->GetElement("scale");
  Scale->Set(_scale);
  sdf::ElementPtr interpl_x = aniElem->GetElement("interpolate_x");
  interpl_x->Set(_interPL);
 
}
*/
  