cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

find_package(gazebo REQUIRED)
include_directories(${GAZEBO_INCLUDE_DIRS})
link_directories(${GAZEBO_LIBRARY_DIRS})
list(APPEND CMAKE_CXX_FLAGS "${GAZEBO_CXX_FLAGS}")

add_library(hello_world SHARED hello_world.cc)
target_link_libraries(hello_world ${GAZEBO_LIBRARIES})

add_library(model_push SHARED model_push.cc)
target_link_libraries(model_push ${GAZEBO_LIBRARIES})

add_library(Actor_Plugin SHARED Actor_Plugin.cc)
target_link_libraries(Actor_Plugin ${GAZEBO_LIBRARIES})

#add_library(Actor_Plugin_BVH SHARED Actor_Plugin_BVH.cc)
#target_link_libraries(Actor_Plugin_BVH ${GAZEBO_LIBRARIES})

add_library(AddAnimBVH SHARED AddAnimBVH.cc)
target_link_libraries(AddAnimBVH ${GAZEBO_LIBRARIES})

add_library(ActorPlugin1 SHARED ActorPlugin1.cc)
target_link_libraries(ActorPlugin1 ${GAZEBO_LIBRARIES})

add_library(ActorPlugin2 SHARED ActorPlugin2.cc)
target_link_libraries(ActorPlugin2 ${GAZEBO_LIBRARIES})

#add_library(Add SHARED Add.cc)
#target_link_libraries(Add ${GAZEBO_LIBRARIES})



#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GAZEBO_CXX_FLAGS}")


# Build the stand-alone test program
add_executable(setTarget setTarget.cc)

if (${gazebo_VERSION_MAJOR} LESS 6)
  # These two
  include(FindBoost)
  find_package(Boost ${MIN_BOOST_VERSION} REQUIRED system filesystem regex)
  target_link_libraries(setTarget ${GAZEBO_LIBRARIES} ${Boost_LIBRARIES})
else()
  target_link_libraries(setTarget ${GAZEBO_LIBRARIES})
endif()


add_executable(setTarget2 setTarget2.cc)

if (${gazebo_VERSION_MAJOR} LESS 6)
  # These two
  include(FindBoost)
  find_package(Boost ${MIN_BOOST_VERSION} REQUIRED system filesystem regex)
  target_link_libraries(setTarget2 ${GAZEBO_LIBRARIES} ${Boost_LIBRARIES})
else()
  target_link_libraries(setTarget2 ${GAZEBO_LIBRARIES})
endif()
