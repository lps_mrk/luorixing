/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <functional>

#include <ignition/math.hh>
#include "gazebo/physics/physics.hh"
#include "Actor_Plugin.hh"
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

using namespace gazebo;
GZ_REGISTER_MODEL_PLUGIN(Actor_Plugin)

#define WALKING_ANIMATION "walking"
using namespace std;
/////////////////////////////////////////////////
Actor_Plugin::Actor_Plugin()
{
}

/////////////////////////////////////////////////
void Actor_Plugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) //load actor plugin
{
  this->sdf = _sdf;   // pointer to the plugin's sdf's elements:<plugin name="actor1_plugin" filename="libActorPlugin.so">
  this->actor = boost::dynamic_pointer_cast<physics::Actor>(_model);  //_model pointer to the parent model:<actor name="actor1">
  this->world = this->actor->GetWorld();
  this->model = _model;
  this->target = ignition::math::Vector3d(10.0, 15.0, 1.2138);

  this->connections.push_back(event::Events::ConnectWorldUpdateBegin(
          std::bind(&Actor_Plugin::OnUpdate, this, std::placeholders::_1)));
  

  // Create the node
      this->node = transport::NodePtr(new transport::Node());
      
      this->node->Init(this->model->GetWorld()->Name());
      

      // Create a topic name
      std::string topicName = "~/" + this->model->GetName() + "/target_cmd";

      // Subscribe to the topic, and register a callback
      this->sub = this->node->Subscribe(topicName,
      &Actor_Plugin::OnMsg, this);
/* 
  this->velocity = 0.1;
  this->lastUpdate = 0;
  
  
  auto skelAnims = this->actor->SkeletonAnimations();

  if (skelAnims.find(WALKING_ANIMATION) == skelAnims.end())
  {
    gzerr << "Skeleton animation " << WALKING_ANIMATION << " not found.\n";
  }
  else
  {
    // Create custom trajectory
    this->trajectoryInfo.reset(new physics::TrajectoryInfo());
    this->trajectoryInfo->type = WALKING_ANIMATION;
    this->trajectoryInfo->duration = 1.0;

    this->actor->SetCustomTrajectory(this->trajectoryInfo);
  }
  
  this->Reset();*/

  // Read in the target weight
 /*  if (_sdf->HasElement("target_weight"))
    this->targetWeight = _sdf->Get<double>("target_weight");
  else
    this->targetWeight = 1.15;

  // Read in the obstacle weight
  if (_sdf->HasElement("obstacle_weight"))
    this->obstacleWeight = _sdf->Get<double>("obstacle_weight");
  else
    this->obstacleWeight = 1.5;
*/
  // Read in the animation factor (applied in the OnUpdate function).
  if (_sdf->HasElement("animation_factor"))
    this->animationFactor = _sdf->Get<double>("animation_factor");
  else
    this->animationFactor = 4.5;
  

  // Add our own name to models we should ignore when avoiding obstacles.
 // this->ignoreModels.push_back(this->actor->GetName());
/*
  // Read in the other obstacles to ignore
  if (_sdf->HasElement("ignore_obstacles"))
  {
    sdf::ElementPtr modelElem =
      _sdf->GetElement("ignore_obstacles")->GetElement("model");
    while (modelElem)
    {
      this->ignoreModels.push_back(modelElem->Get<std::string>());
      modelElem = modelElem->GetNextElement("model");
    }
  }
   */
}

///////////

//void Actor_Plugin::SetTarget(ignition::math::Vector3d &_target1)
void Actor_Plugin::SetTarget(const double &_target1)
{
  this->target.X() = _target1;
}

/// \brief Handle incoming message
/// \param[in] _msg Repurpose a vector3 message. This function will
/// only use the x component.
void Actor_Plugin::OnMsg(ConstVector3dPtr &_msg)
{
  this->SetTarget(_msg->x());
}



void Actor_Plugin::OnUpdate(const common::UpdateInfo &_info)
{
  // Time delta
  double dt = (_info.simTime - this->lastUpdate).Double();

  ignition::math::Pose3d pose = this->actor->WorldPose();
  ignition::math::Vector3d pos = this->target - pose.Pos();
  ignition::math::Vector3d rpy = pose.Rot().Euler();

  double distance = pos.Length();


  

  // Choose a new target position if the actor has reached its current
  // target.
  /* 
  if (distance < 0.1)
  {
    this->SetTarget();
    pos = this->target - pose.Pos();
  }
*/
   //Normalize the direction vector, and apply the target weight
   pos = pos.Normalize();

  // Adjust the direction vector by avoiding obstacles
 // this->HandleObstacles(pos);

  // Compute the yaw orientation
  //ignition::math::Angle yaw = atan2(pos.Y(), pos.X()) + 1.5707 - rpy.Z();//尝试把1.5707或者rpy.Z()删掉，会出现奇怪现象
  ignition::math::Angle yaw = atan2(pos.Y(), pos.X()) + 1.5707 - rpy.Z(); //正常情况下是atan2(x,y),而这里是atan2(y,x)
  yaw.Normalize();

   pose.Pos() += pos * this->velocity * dt;
   pose.Rot() = ignition::math::Quaterniond(1.5707, 0, rpy.Z()+yaw.Radian());
  // Rotate in place, instead of jumping.
 /*  if (std::abs(yaw.Radian()) > IGN_DTOR(10))  //这句应该没什么用，所以把他改写成上面两句
  {
    pose.Rot() = ignition::math::Quaterniond(1.5707, 0, rpy.Z()+
        yaw.Radian()*0.001);
  }
  else
  {
    pose.Pos() += pos * this->velocity * dt;
    pose.Rot() = ignition::math::Quaterniond(1.5707, 0, rpy.Z()+yaw.Radian());
  }

*/
 /*  // Make sure the actor stays within bounds
  pose.Pos().X(std::max(-30.0, std::min(35.0, pose.Pos().X())));
  pose.Pos().Y(std::max(-20.0, std::min(20.0, pose.Pos().Y())));
  pose.Pos().Z(1.2138);
*/
  // Distance traveled is used to coordinate motion with the walking
  // animation
  double distanceTraveled = (pose.Pos() -
      this->actor->WorldPose().Pos()).Length();

  this->actor->SetWorldPose(pose, false, false);
  this->actor->SetScriptTime(this->actor->ScriptTime() +
    (distanceTraveled * this->animationFactor));
  this->lastUpdate = _info.simTime;
  //cout<<distance<<endl;
  //cout<<this->actor->WorldPose()<<endl;
  cout<<"using onupdate"<<endl;
}

/////////////////////////////////////////////////
void Actor_Plugin::Reset()
{
  cout<<"using reset"<<endl;
  this->velocity = 0.2;
  this->lastUpdate = 0;

  if (this->sdf && this->sdf->HasElement("target"))
    this->target = this->sdf->Get<ignition::math::Vector3d>("target");
  else
    this->target = ignition::math::Vector3d(10.0, 15.0, 1.2138);

  auto skelAnims = this->actor->SkeletonAnimations();
  if (skelAnims.find(WALKING_ANIMATION) == skelAnims.end())
  {
    gzerr << "Skeleton animation " << WALKING_ANIMATION << " not found.\n";
  }
  else
  {
    // Create custom trajectory
    this->trajectoryInfo.reset(new physics::TrajectoryInfo());
    this->trajectoryInfo->type = WALKING_ANIMATION;
    this->trajectoryInfo->duration = 1.0;

    this->actor->SetCustomTrajectory(this->trajectoryInfo);
  }
}



/* 
/////////////////////////////////////////////////
void Actor_Plugin::ChooseNewTarget()
{
  ignition::math::Vector3d newTarget(this->target);//???newTarget= this->target,看后面Onupdate调用了ChooseNewTarget(),那既然调用了该函数，那肯定是得重新选择新目标，设置成这样，就满足选新目标的要求了
  while ((newTarget - this->target).Length() < 2.0)
  {
    newTarget.X(ignition::math::Rand::DblUniform(-30.0, 35.0));
    newTarget.Y(ignition::math::Rand::DblUniform(-30.0, 20.0));

    for (unsigned int i = 0; i < this->world->ModelCount(); ++i)
    {
      double dist = (this->world->ModelByIndex(i)->WorldPose().Pos()
          - newTarget).Length();
      if (dist < 2.0)
      {
        newTarget = this->target;
        break;
      }
    }
  }
  this->target = newTarget;
}
*/
/* 
/////////////////////////////////////////////////
void Actor_Plugin::HandleObstacles(ignition::math::Vector3d &_pos)//???没有return,也没有设置什么？ _pos vector direction to target
{
  for (unsigned int i = 0; i < this->world->ModelCount(); ++i)
  {
    physics::ModelPtr model = this->world->ModelByIndex(i);
    if (std::find(this->ignoreModels.begin(), this->ignoreModels.end(),
          model->GetName()) == this->ignoreModels.end()) //找出不属于ignoreModels的model
    {
      ignition::math::Vector3d offset = model->WorldPose().Pos() -
        this->actor->WorldPose().Pos();//offset 障碍物与actor之间的vector
      double modelDist = offset.Length();
      if (modelDist < 4.0)
      {
        double invModelDist = this->obstacleWeight / modelDist;
        offset.Normalize();
        offset *= invModelDist;
        _pos -= offset;
      }
    }
  }
}
*/

/////////////////////////////////////////////////

